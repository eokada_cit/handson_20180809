webpackJsonp([1],{

/***/ 268:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdicionarMusicaPageModule", function() { return AdicionarMusicaPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__adicionar_musica__ = __webpack_require__(270);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AdicionarMusicaPageModule = /** @class */ (function () {
    function AdicionarMusicaPageModule() {
    }
    AdicionarMusicaPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__adicionar_musica__["a" /* AdicionarMusicaPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__adicionar_musica__["a" /* AdicionarMusicaPage */]),
            ],
        })
    ], AdicionarMusicaPageModule);
    return AdicionarMusicaPageModule;
}());

//# sourceMappingURL=adicionar-musica.module.js.map

/***/ }),

/***/ 270:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdicionarMusicaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(49);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the AdicionarMusicaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AdicionarMusicaPage = /** @class */ (function () {
    function AdicionarMusicaPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.musica = [{ nome: "Nome 1", artista: "João" }, { nome: "Nome 2", artista: "Maria" }];
    }
    AdicionarMusicaPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AdicionarMusicaPage');
    };
    AdicionarMusicaPage.prototype.getItems = function (event) {
        var val = event.target.value;
        if (val < 3) {
        }
    };
    AdicionarMusicaPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-adicionar-musica',template:/*ion-inline-start:"/Users/hugorafael/Documents/handon/handson_20180809/ionic/my-music/src/pages/adicionar-musica/adicionar-musica.html"*/'<!--\n  Generated template for the AdicionarMusicaPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Adicionar Música</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content>\n    <ion-searchbar (ionInput)="getItems($event)"></ion-searchbar>\n    <ion-list no-lines>\n        <ion-item *ngFor="let m of musica">\n          <h2>{{m.nome}}</h2>\n          <p>{{m.artista}}</p>\n          \n          <button ion-button item-end>\n              <ion-icon name="trash"></ion-icon>\n          </button> \n        </ion-item>\n    </ion-list>\n</ion-content>\n'/*ion-inline-end:"/Users/hugorafael/Documents/handon/handson_20180809/ionic/my-music/src/pages/adicionar-musica/adicionar-musica.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */]])
    ], AdicionarMusicaPage);
    return AdicionarMusicaPage;
}());

//# sourceMappingURL=adicionar-musica.js.map

/***/ })

});
//# sourceMappingURL=1.js.map