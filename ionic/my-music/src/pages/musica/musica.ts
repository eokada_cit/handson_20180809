import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the MusicaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage({
  name: 'musica'
})
@Component({
  selector: 'page-musica',
  templateUrl: 'musica.html',
})
export class MusicaPage {

  musica = [{nome: "Nome 1", artista: "João"}, {nome: "Nome 2", artista: "Maria"}];

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MusicaPage');
  }
  
  adicionarMusica(){
    this.navCtrl.push('adicionar-musica');
  }
}
