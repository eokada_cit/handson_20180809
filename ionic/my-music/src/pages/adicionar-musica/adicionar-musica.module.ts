import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdicionarMusicaPage } from './adicionar-musica';

@NgModule({
  declarations: [
    AdicionarMusicaPage,
  ],
  imports: [
    IonicPageModule.forChild(AdicionarMusicaPage),
  ],
})
export class AdicionarMusicaPageModule {}
