package com.mycompany.mymusic.repository;

import org.springframework.data.repository.CrudRepository;

import com.mycompany.mymusic.model.Playlists;

public interface PlaylistRepository extends CrudRepository<Playlists, String>{
	
	

}
