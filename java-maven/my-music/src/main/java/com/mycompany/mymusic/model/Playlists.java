package com.mycompany.mymusic.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name = "Playlists")
public class Playlists {

	@Id
	private String id;
//	private Iterable<PlaylistMusicas> playlistMusicas;
//	private Usuario usuario;
	
	
	
	public Playlists() {
		super();
	}
	public Playlists(String id) {
		super();
		this.id = id;
	}
//	public Playlists(String id, Iterable<PlaylistMusicas> playlistMusicas, Usuario usuario) {
//		super();
//		this.id = id;
//		this.playlistMusicas = playlistMusicas;
//		this.usuario = usuario;
//	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
//	public Iterable<PlaylistMusicas> getPlaylistMusicas() {
//		return playlistMusicas;
//	}
//	public void setPlaylistMusicas(Iterable<PlaylistMusicas> playlistMusicas) {
//		this.playlistMusicas = playlistMusicas;
//	}
//	public Usuario getUsuario() {
//		return usuario;
//	}
//	public void setUsuario(Usuario usuario) {
//		this.usuario = usuario;
//	}
	
	
}
