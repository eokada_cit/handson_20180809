package com.mycompany.mymusic.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mycompany.mymusic.model.Musica;
import com.mycompany.mymusic.model.PlaylistMusicas;
import com.mycompany.mymusic.model.Playlists;
import com.mycompany.mymusic.model.Usuario;
import com.mycompany.mymusic.repository.MusicasRepository;
import com.mycompany.mymusic.repository.PlaylistMusicasRepository;
import com.mycompany.mymusic.repository.PlaylistRepository;
import com.mycompany.mymusic.repository.UsuarioRepository;

@Service
public class MusicaService {
	
	@Autowired
	private MusicasRepository musicasRepository;
	
	public void adicionar(Musica musica) {
		
		musicasRepository.save(musica);
		
	}
	
}
