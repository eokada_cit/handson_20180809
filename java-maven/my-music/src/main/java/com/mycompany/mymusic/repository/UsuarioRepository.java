package com.mycompany.mymusic.repository;

import org.springframework.data.repository.CrudRepository;

import com.mycompany.mymusic.model.Usuario;

public interface UsuarioRepository extends CrudRepository<Usuario, String>{
	
	

}
