package com.mycompany.mymusic.repository;

import org.springframework.data.repository.CrudRepository;

import com.mycompany.mymusic.model.PlaylistMusicas;
import com.mycompany.mymusic.model.Playlists;

public interface PlaylistMusicasRepository extends CrudRepository<PlaylistMusicas, String>{
	
	

}
