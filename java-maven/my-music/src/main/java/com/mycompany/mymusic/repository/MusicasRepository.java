package com.mycompany.mymusic.repository;

import org.springframework.data.repository.CrudRepository;

import com.mycompany.mymusic.model.Musica;

public interface MusicasRepository extends CrudRepository<Musica, String>{
	
	

}
