package com.mycompany.mymusic.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name = "Musicas")
public class Musica {
	
	@Id
	private String id;
	private String nome;
	private String artistaId;
	
	public Musica() {
		super();
	}

	public Musica(String id, String nome, String artistaId) {
		super();
		this.id = id;
		this.nome = nome;
		this.artistaId = artistaId;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getArtistaId() {
		return artistaId;
	}
	public void setArtistaId(String artistaId) {
		this.artistaId = artistaId;
	}
	
	

}
