package com.mycompany.mymusic.model;

public class PlaylistMusicas {
	
	private String playlistId;
	private String musicaId;
//	private Musica musica;
	
	public PlaylistMusicas() {
		super();
	}
//	public PlaylistMusicas(String playlistId, String musicaId, Musica musica) {
//		super();
//		this.playlistId = playlistId;
//		this.musicaId = musicaId;
//		this.musica = musica;
//	}
	public PlaylistMusicas(String playlistId, String musicaId) {
		super();
		this.playlistId = playlistId;
		this.musicaId = musicaId;
	}
	public String getPlaylistId() {
		return playlistId;
	}
	public void setPlaylistId(String playlistId) {
		this.playlistId = playlistId;
	}
	public String getMusicaId() {
		return musicaId;
	}
	public void setMusicaId(String musicaId) {
		this.musicaId = musicaId;
	}
//	public Musica getMusica() {
//		return musica;
//	}
//	public void setMusica(Musica musica) {
//		this.musica = musica;
//	}
	


}
