package com.mycompany.mymusic.model;

public class Usuario {
	
//	"usuario": {
//    "id": "string",
//    "nome": "string",
//    "playlistId": "string"
//  }
	
	private String id;
	private String nome;
	private String playlistId;
	
	
	
	public Usuario() {
		super();
	}
	public Usuario(String id, String nome, String playlistId) {
		super();
		this.id = id;
		this.nome = nome;
		this.playlistId = playlistId;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getPlaylistId() {
		return playlistId;
	}
	public void setPlaylistId(String playlistId) {
		this.playlistId = playlistId;
	}
	
	


}
