package com.mycompany.mymusic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mycompany.mymusic.model.Musica;
import com.mycompany.mymusic.service.MusicaService;

@Controller
public class MusicaController {
	
	@Autowired
	private MusicaService service;
	
	//@RequestMapping(value = "/api/playlists/musicas", method = RequestMethod.PUT)
	@PutMapping("/api/playlists/musicas")
	@ResponseBody
	public void adicionar(@RequestBody Musica musica) {

		service.adicionar(musica);
	}

}
